STARTUP_COMMAND = "import-files"
IMAGE_NAME = "andreclaudino/postgres-importer"
IMAGE_TAG = "1.0.0"


DISPLAY_NAME = "Load into Postgresql"

MEMORY_REQUEST = "100Mi"
MEMORY_LIMIT = "200Mi"

CPU_REQUEST = "500m"
CPU_LIMIT = "1000m"

LOGLEVEL = "debug"
S3_ENDPOINT = "http://minio.10.0.0.200.nip.io:80"
AWS_ACCESS_KEY_ID = "minio"
AWS_SECRET_ACCESS_KEY = "minio123"


DEFAULT_FILE_TYPE = "JSON"
DEFAULT_DB_HOST = "postgres.storage.svc"
DEFAULT_DB_PORT = 5432
DEFAULT_DB_USERNAME = "postgres"
DEFAULT_DB_PASSWORD = "postgres"
DEFAULT_DB_NAME = "data_lake"
DEFAULT_DB_SCHEMA = "public"
DEFAULT_DB_TABLE = ""
DEFAULT_BATCH_SIZE = 64
