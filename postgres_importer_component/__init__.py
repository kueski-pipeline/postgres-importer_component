from kfp import dsl
from kfp.dsl import ContainerOp

from postgres_importer_component.configure import setup_resources, add_environments
from postgres_importer_component.constants import DEFAULT_FILE_TYPE, DEFAULT_DB_HOST, DEFAULT_DB_PORT, \
    DEFAULT_DB_USERNAME, DEFAULT_DB_PASSWORD, DEFAULT_DB_NAME, DEFAULT_DB_SCHEMA, DEFAULT_DB_TABLE, DEFAULT_BATCH_SIZE, \
    STARTUP_COMMAND, IMAGE_NAME, IMAGE_TAG, CPU_LIMIT, CPU_REQUEST, MEMORY_LIMIT, DISPLAY_NAME, MEMORY_REQUEST


@dsl.component
def make_postgres_importer(
        file_type: str = DEFAULT_FILE_TYPE, source: str = "", db_host: str = DEFAULT_DB_HOST,
        db_port: int = DEFAULT_DB_PORT, db_username: str = DEFAULT_DB_USERNAME, db_password: str = DEFAULT_DB_PASSWORD,
        db_name: str = DEFAULT_DB_NAME, db_schema: str = DEFAULT_DB_SCHEMA,  db_table: str = DEFAULT_DB_TABLE,
        batch_size: int = DEFAULT_BATCH_SIZE,

        command: str = STARTUP_COMMAND, image_name: str = IMAGE_NAME, image_tag: str = IMAGE_TAG,
        display_name: str = DISPLAY_NAME, memory_request: int = MEMORY_REQUEST,
        memory_limit: int = MEMORY_LIMIT, cpu_request: int = CPU_REQUEST, cpu_limit: int = CPU_LIMIT):

    image = f"{image_name}:{image_tag}"
    operator = ContainerOp(
        image=image,
        name="awin_feed",
        command=command,
        arguments=[
            "--file-type", file_type,
            "-source", source,
            "--db-host", db_host,
            "--db-port", db_port,
            "--db-username", db_username,
            "--db-password", db_password,
            "--db-name", db_name,
            "--db-schema", db_schema,
            "--db-table", db_table,
            "--batch-size", batch_size
        ]
    )

    operator = operator.set_display_name(display_name)
    operator = setup_resources(operator, memory_request, memory_limit, cpu_request, cpu_limit)
    operator = add_environments(operator)
    operator.execution_options.caching_strategy.max_cache_staleness = "P0D"

    return operator
