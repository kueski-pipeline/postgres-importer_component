from setuptools import setup, find_packages

setup(
    name='postgres-importer-component',
    version='1.0.1',
    packages=find_packages(),
    url='',
    license='',
    author='',
    author_email='',
    description='KFP component to import files from object storage to postgres',
    install_requires=[
        "kfp==1.4.0"
    ]
)
